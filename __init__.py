import pandas as pd
import chromadb

chroma_client = chromadb.Client()

collection = chroma_client.create_collection(name="my_collection")

document_list = []
metadata_list = []

# Leer datos del csv
df = pd.read_csv('atom_metadata.csv')

# Funcion que crea una descripcion para los casos que no la tienen
def get_description(doc):
  # Si no tiene description, nos quedamos con el titulo + identificador
  description = doc['doc_description']
  if pd.isna(description) or description == "-":
     return doc['title'] if pd.isna(doc['identifier']) else f"{doc['identifier']},{doc['title']}"

  # Sino, devolvemos descripcion
  return description.replace("\n", "")

# Aplicar funcion anterior
df['doc_description'] = df.apply(get_description, axis=1)

df = df.drop_duplicates(subset=["identifier"])

for index, row in df.iterrows():
    document_list.append(row['doc_description'])
    metadata_list.append({
        "title": row['title'],
        "format": row['format_description'],
        "date": row['event_date'],
        "image": row['image_url'],
        "url": row['doc_url'],
    })

collection.add(
    documents=document_list,
    metadatas=metadata_list,
)





